# Pbt

Property based testing from the book Property Based Testing with PropEr,
Erlang, and Elixir

# Chapter 1 Exercises

Question 1:

What function can you call to get a sample of generators data?

An example in iex

```elixir
{:ok list} = PropCheck.BasicTypes.int
|> PropCheck.BasicTypes.list
|> PropCheck.produce(10)
```

or make it easy and

```elixir
use PropCheck

int |> list |> non_empty |> produce
```

generate alot of samples

```elixir
use PropCheck

for x <- 1..100 do
  int |> list |> non_empty |> produce
end
```


