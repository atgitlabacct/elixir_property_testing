##
# To test this just do mix run lib/exercises/ch1.exs
#
# This is testing that the length of the list is the same value as the count
# of elements including the start value and also checking that the list is
# incremented by one in the next position.
ExUnit.start()

defmodule Exercises.Ch2 do
  use ExUnit.Case
  use PropCheck

  property "exercise 2: a sample" do
    forall {start, count} <- {integer(), non_neg_integer()} do
      list = Enum.to_list(start..(start + count))
      count + 1 == length(list) and increments(list)
    end
  end

  def increments([head | tail]), do: increments(head, tail)

  defp increments(_, []), do: true

  defp increments(n, [head | tail]) when n + 1 == head,
    do: increments(head, tail)

  defp increments(_, _), do: false
end
