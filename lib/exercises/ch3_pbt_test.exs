##
# Using modeling to property test.
ExUnit.start()

defmodule Ch3PbtTest do
  use ExUnit.Case
  use PropCheck

  property "finds the biggest element" do
    forall x <- non_empty(list(integer())) do
      biggest(x) == model_biggest(x)
    end
  end

  def biggest([head | tail]) do
    biggest(tail, head)
  end

  defp biggest([], max), do: max

  defp biggest([head | tail], max) when head >= max, do: biggest(tail, head)
  defp biggest([head | tail], max) when head < max, do: biggest(tail, max)

  defp model_biggest(x) do
    # This is the same this we did in pbt_test.exs initially.
    List.last(Enum.sort(x))
  end
end
