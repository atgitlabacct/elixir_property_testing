# Chapter 3 Thinking in Properties

## Tips/Tricks for writing Prop tests

- First try modeling our code
- Second if modeling doesn't work we will try generalizing properties out of
  traditional example based cases
- Next we will try finding invariants.  Go from trivial to solid test suite.
- Lastly symmetric props for specific problems.

### Modeling

- For code that does a conceptually simple thing, modeling is useful.
- Boils down to piting a very simple implementation, often algorithimcally
  inefficient one against the real implementation.  Then optimize the real one.

As an example in ch2.exs the biggest function is so trivial the properties we
come up to test would more then likely reimplement our implementation.  In
these cases modeling is a good idea.We need to come up with an alternative
implementation that is correct.

As a generaly rule when using modeling, we can assume our code implementation
is going to be as reliable as the model to which we compare it.  Thats why we
aim to have models as so simple they are obviously corect.

Integration tests for stateful systems with lots of side effects/dependencies
can often use modeling.

A rare but great type of modeling is called an Oracle.  Its basically a
reference implementation or lib/3rd party package that you can test against to
make sure you get the right answer.

### Generalizing Example Tests

Start writing a regular unit test and then abstract it away.  Come up with all
individual examples and replace them with generators
