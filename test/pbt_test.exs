defmodule PbtTest do
  use ExUnit.Case
  use PropCheck
  doctest Pbt

  property "description of what the property does" do
    forall type <- my_type() do
      boolean(type)
    end
  end

  def boolean(_) do
    true
  end

  def my_type() do
    term()
  end

  property "finds the biggest element" do
    forall x <- non_empty(list(integer())) do
      biggest(x) == List.last(Enum.sort(x))
    end
  end

  def biggest([head | tail]) do
    biggest(tail, head)
  end

  defp biggest([], max), do: max

  defp biggest([head | tail], max) when head >= max, do: biggest(tail, head)
  defp biggest([head | tail], max) when head < max, do: biggest(tail, max)
end
